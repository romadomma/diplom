from model import f,h,g,F,PSI,H,GAM
import numpy as np

#Fetch matrix Pa
def fetchPa(P, Q, R):
    lenP = len(P)
    lenQ = len(Q)
    lenR = len(R)
    Pa = np.zeros((lenP+lenQ+lenR,lenP+lenQ+lenR))
    if(lenP > 1): 
        for i in range(lenP):
            for j in range(lenP):
                Pa[i][j] = P[i][j]
    else:
        Pa[0][0] = P[0]
    if(lenQ > 1):        
        for i in range(lenQ):
            for j in range(lenQ):
                Pa[i+lenP][j+lenP] = Q[i][j]
    else:
        Pa[lenP][lenP] = Q[0]
    if(lenR > 1):  
        for i in range(lenR):
            for j in range(lenR):
                Pa[i+lenP+lenQ][j+lenP+lenQ] = R[i][j]
    else:
        Pa[lenP+lenQ][lenP+lenQ] = R[0]
    return Pa

#Fetch vector Xa
def fetchXa(x,w,v,Nx,Ny):
    Xa = np.zeros(Nx+Nx+Ny)
    for i in range(Nx):
        Xa[i] = x[i] if not isinstance(x, (float)) else x
    for i in range(Nx):
        Xa[Nx + i] = w[i] if not isinstance(w, (float)) else w
    for i in range(Ny):
        Xa[Nx + Nx + i] = v[i] if not isinstance(v, (float)) else v
    return Xa

def ExtendedFilter(Ymodel, r, R, q, Q, Ex, P, N, Ut, Nx, Ny, param = 1):
    b = 0.998
    Bt = np.zeros((N,Ny,Ny))
    Xt = np.zeros((N,1,Nx))
    Xt[0] = Ex
    Pt = np.zeros((N,Nx,Nx))
    Pt[0] = P
    Qt = np.zeros((N,Nx,Nx))
    Qt[0] = Q
    Rt = np.zeros((N,Ny,Ny))
    Rt[0] = R
    Kt = np.zeros((N,Nx,Ny))
    Yresult = np.zeros((N,1,Ny))
    Yresult[0] = h(Xt[0])
    eps = np.zeros((N,1,Ny))
    for k in range(N-1):
        Xt[k+1] = f(Xt[k],Ut[k],param=param)
        Pt[k+1] = np.dot(np.dot(F(Xt[k],Ut[k],q,param=param),Pt[k]),F(Xt[k],Ut[k],q,param=param).transpose()) + np.dot(np.dot(PSI(Xt[k+1],Ut[k+1],q),Qt[0]),PSI(Xt[k+1],Ut[k+1],q).transpose())
        
        Bt[k+1] = np.dot(np.dot(H(Xt[k+1], r), Pt[k+1]), H(Xt[k+1], r).transpose()) + R[0]

        d = (1-b)/(1-b**k+1)
        eps[k+1] = Ymodel[k+1] - h(Xt[k+1])
        E = np.dot(np.linalg.inv(np.dot(GAM(Xt[k+1], r).transpose(), GAM(Xt[k+1], r))), GAM(Xt[k+1], r).transpose())
        ##++##
        Rt[k+1] = (1-d)*Rt[k]+d*np.dot((np.eye(Ny)-np.dot(H(Xt[k+1], r), Kt[k+1])), np.dot(np.dot(eps[k+1], eps[k+1].transpose()), (np.eye(Ny)-np.dot(H(Xt[k+1], r), Kt[k+1])).transpose())+np.dot(np.dot(H(Xt[k+1], r), Pt[k+1]),H(Xt[k+1], r).transpose()))
        
        if Ny == 1:
            Qt[k+1] = (1-d)*Qt[k]+np.dot(np.dot(d*E[:,0][0], np.dot(np.dot(np.dot(Kt[k+1], eps[k+1].transpose()), eps[k+1]), Kt[k+1].transpose())+Pt[k+1]-np.dot(np.dot(F(Xt[k],Ut[k],q,param=param),Pt[k]),F(Xt[k],Ut[k],q).transpose())), E[:,0][0])
        ##+-##
        else:
            Qt[k+1] = (1-d)*Qt[k]+np.dot(np.dot(d*E, np.dot(np.dot(np.dot(Kt[k+1], eps[k+1].transpose()), eps[k+1]), Kt[k+1].transpose())+Pt[k+1]-np.dot(np.dot(F(Xt[k],Ut[k],q,param=param),Pt[k]),F(Xt[k],Ut[k],q,param=param).transpose())), E.transpose())

        Kt[k+1] = np.dot(np.dot(Pt[k+1], H(Xt[k+1], r).transpose()), np.linalg.inv(np.dot(np.dot(H(Xt[k+1], r), Pt[k+1]),H(Xt[k+1], r).transpose()) + np.dot(np.dot(GAM(Xt[k+1], r).transpose(), Rt[0]), GAM(Xt[k+1], r))))
        Xt[k+1] += np.dot(Kt[k+1], eps[k+1]).transpose()
        Pt[k+1] = np.dot(np.eye(Nx) - np.dot(Kt[k+1], H(Xt[k+1], r)), Pt[k+1])
        Yresult[k+1] = h(Xt[k+1])
    return {'x': Xt, 'y': Yresult, 'eps': eps, 'Pyy': Bt}

#Kalman unscented filter
def UnscentedFilter(Ymodel, r, R, q, Q, Ex, P, N, Ut, Nx, Ny, param = 1):
    #Init dim and const
    alpha = 0.0001
    betta = 2.0
    k = 0.0
    b = 0.998
    lam = alpha * alpha * (Nx + k) - Nx
    gamma = Nx + lam
    #Calc weight
    Wm = np.zeros(2*Nx +1)
    Wc = np.zeros(2*Nx +1)
    Wm[0] = lam/gamma 
    Wc[0] = lam/gamma + (1 - alpha*alpha + betta)
    for i in range(1,2*Nx+1):
        Wm[i] =  1/(2*gamma)
        Wc[i] =  Wm[i]
    #Init matrix
    Xt = np.zeros((N,1,Nx))
    Xt[0] = Ex
    Pt = np.zeros((N,Nx,Nx))
    Pt[0] = P
    Qt = np.zeros((N,Nx,Nx))
    Qt[0] = Q
    Rt = np.zeros((N,Ny,Ny))
    Rt[0] = R

    Xsigm = np.zeros((N, 2*Nx+1, 1,Nx))
    Xsigm_x = np.zeros((N, 2*Nx+1, 1, Nx))
    Ysigm = np.zeros((N, 2*Nx+1, 1, Ny))
    Yt = np.zeros((N,1,Ny))
    Yresult = np.zeros((N,1,Ny))
    Yresult[0] = h(Xt[0])
    eps = np.zeros((N,1,Ny))
    Pyy = np.zeros((N,Ny,Ny))
    Pxy = np.zeros((N,Nx,Ny))
    Kt = np.zeros((N,Nx,Ny))
    #General loop
    for k in range(N-1):
        #Calc sigma-points

        Asqrt = np.sqrt(gamma)*np.linalg.cholesky(Pt[k])
        Xsigm[k][0] = Xt[k]
        for i in range(1,Nx+1):
            Xsigm[k][i] = Xt[k] + Asqrt[:, i-1]
            Xsigm[k][i+Nx] = Xt[k] - Asqrt[:, i-1]
        
        #Calc X and P
        for i in range(2*Nx+1):
            Xsigm_x[k+1][i] = f(Xsigm[k][i], Ut[k], param = param)
        for i in range(2*Nx+1):
            Xt[k+1] += Wm[i]*Xsigm_x[k+1][i]
        for i in range(2*Nx+1):
            Pt[k+1] += Wc[i]*np.dot((Xsigm_x[k+1][i] - Xt[k+1]).transpose(), Xsigm_x[k+1][i] - Xt[k+1])
        Pt[k+1] += g()*Qt[k]*g().transpose()
        for i in range(2*Nx+1):
            Ysigm[k+1][i] = h(Xsigm_x[k+1][i])
        for i in range(2*Nx+1):
            Yt[k+1] += Wm[i]*Ysigm[k+1][i]

        #Update R
        d = (1-b)/(1-b**k+1)
        eps[k+1] = Ymodel[k+1] - Yt[k+1]

        for i in range(2*Nx+1):
            Pyy[k+1] += Wc[i]*np.dot((Ysigm[k+1][i] - Yt[k+1]).transpose(), Ysigm[k+1][i] - Yt[k+1])
        
        Rt[k+1] = (1-d)*Rt[k]+d*(np.dot(eps[k+1].transpose(), eps[k+1]) - Pyy[k+1])
        
        Pyy[k+1] += Rt[k+1]

        for i in range(2*Nx+1):
            Pxy[k+1] += Wc[i]*np.dot((Xsigm_x[k+1][i] - Xt[k+1]).transpose(), Ysigm[k+1][i] - Yt[k+1])
        Kt[k+1] = np.dot(Pxy[k+1],np.linalg.inv(Pyy[k+1]))

        for i in range(2*Nx+1):
            Qt[k+1] += Wc[i]*np.dot((Xsigm_x[k+1][i] - Xt[k+1]).transpose(), Xsigm_x[k+1][i] - Xt[k+1])
        Qt[k+1] = (1-d)*Qt[k]+d*(np.dot(np.dot(Kt[k+1],np.dot(eps[k+1].transpose(), eps[k+1])), Kt[k+1].transpose())+Pt[k]-Qt[k+1])

        #Update X and P
        Xt[k+1] += np.dot(Kt[k+1], eps[k+1].transpose()).transpose()
        Pt[k+1] -= np.dot(Kt[k+1],np.dot(Kt[k+1], Pyy[k+1]).transpose())
        Yresult[k+1] = h(Xt[k+1])

    #return filtred x, y, eps and matrix B
    return {'x': Xt, 'y': Yresult, 'eps': eps, 'Pyy': Pyy}