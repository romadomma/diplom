import numpy as np

#sin/cos
def F(Xt, Ut, w, param = 1):
    return np.array([[param*np.cos(param*Xt[0][0]), 1],[0, -np.sin(Xt[0][1])]])

def PSI(Xt, Ut, w):
    return np.array([[1, 0],[0, 1]])

def GAM(Xt, v):
    return np.array([[1]])

def H(Xt, v):
    return np.array([[1, 0]])

def g():
    return np.array([1,1])

def f(Xk, u, Gk = (1,1), w = 1, param = 1):
    X = np.zeros([1,2])
    if isinstance(w,int) and w == 1:
        X[0][0] = np.sin(param*Xk[0][0]) + Xk[0][1] + u*10
        X[0][1] = np.cos(Xk[0][1])
    else:
        X[0][0] = np.sin(param*Xk[0][0]) + Xk[0][1] + u*10 + w[0]
        X[0][1] = np.cos(Xk[0][1]) + w[1]
    return X

def h(Xk, v = 1, param = 1):
    if isinstance(v,int) and v == 1:
        return np.array([Xk[0][0]])
    else:
        return np.array([Xk[0][0]+v])
#1
def F1(Xt, Ut, w, param = (1,1)):
    return np.array([[param[0]*(-2*Xt**2/(1+Xt**2)**2+1/(Xt**2+1))]])

def PSI1(Xt, Ut, w):
    return np.array([[0.7]])

def H1(Xt, v):
    return np.array([[0.6]])

def GAM1(Xt, v):
    return np.array([[0.2]])

def g1():
    return np.array([[0.7]])

def f1(Xk, u, Gk = 0.7, w = 1, param = (1,1)):
    X = np.zeros(1)
    if isinstance(w,int) and w == 1:
        X[0] = param[0]*Xk/(1+Xk**2)
    else:
        X[0] = param[0]*Xk/(1+Xk**2)+Gk*w*param[1]
    return X

def h1(Xk, v = 1, param = (1,1)):
    Y = np.zeros(1)
    if isinstance(v,int) and v == 1:
        Y[0] = 0.6*Xk
    else:
        Y[0] = 0.6*Xk+v
    return Y