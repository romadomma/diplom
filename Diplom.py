﻿import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt

def main():
    global Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy, GYnoise, GYclear, GXnoise, GF, GH, GPSI, GGAM
    init()
    optimal = opt.minimize_scalar(lnL)
    Aoptimal = opt.minimize_scalar(AlnL)
    filtred = Filter(GYnoise, Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GF, GH, GPSI, GGAM, GNx, GNy, param = optimal['x'])
    Afiltred = AFilter(GYnoise, Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GF, GH, GPSI, GGAM, GNx, GNy, param = Aoptimal['x'])
    print(optimal['x'])
    print(1-optimal['x'])
    print(getNorm(filtred['y'][:,0])/np.linalg.norm(GYclear))
    print(Aoptimal['x'])
    print(1-Aoptimal['x'])
    print(getNorm(Afiltred['y'][:,0])/np.linalg.norm(GYclear))
    

def init():
    global Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GF, GH, GPSI, GGAM, GNx, GNy, GYnoise, GYclear, GXnoise 
    Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GF, GH, GPSI, GGAM, GNx, GNy = input("input.txt")
    GYnoise, GYclear, GXnoise = GetModel(Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GF, GH, GPSI, GGAM, GNx, GNy)

def GetModel(r, R, q, Q, Ex, P, N, Ut, F, H, PSI, GAM, Nx, Ny):
    global Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy, GYnoise, GYclear, GXnoise
    Yclear = np.zeros((N,1,Ny))
    Xclear = np.zeros((N,1,Nx))
    Xnoise = np.zeros((N,1,Nx))
    Ynoise = np.zeros((N,1,Ny))
    Xnoise[0] = np.random.multivariate_normal(Ex, P, 1) if Nx > 1 else np.random.normal(Ex, P, 1) 
    Xclear[0] = Xnoise[0]
    Ynoise[0][0] = np.dot(H,Xclear[0].transpose())
    Yclear[0][0] = np.dot(H,Xclear[0].transpose())
    w = np.random.multivariate_normal(q, Q, N) if Nx > 1 else np.random.normal(q, Q, N) 
    v = np.random.multivariate_normal(r, R, N) if Ny > 1 else np.random.normal(r, R, N)

    Qz = np.array([[1, 0], [0, 1]])
    Rz = np.array([1])
    wz = np.random.multivariate_normal(q, Qz, 10) if Nx > 1 else np.random.normal(q, Qz, 10) 
    vz = np.random.multivariate_normal(r, Rz, 10) if Ny > 1 else np.random.normal(r, Rz, 10)
    #1 TEST
    # w[13] = wz[0]
    # w[22] = wz[1]
    # w[29] = wz[2]
    # w[35] = wz[3]
    # w[42] = wz[4]
    # w[50] = wz[5]
    # w[63] = wz[6]
    # w[81] = wz[7]
    # w[88] = wz[8]
    # w[97] = wz[9]
    #2 TEST
    # v[4] = vz[0]
    # v[15] = vz[1]
    # v[19] = vz[2]
    # v[26] = vz[3]
    # v[34] = vz[4]
    # v[46] = vz[5]
    # v[57] = vz[6]
    # v[69] = vz[7]
    # v[74] = vz[8]
    # v[84] = vz[9]
    #3 TEST
    # w[21] = wz[0]
    # w[22] = wz[1]
    # w[23] = wz[2]
    # w[24] = wz[3]
    # w[25] = wz[4]
    # w[67] = wz[5]
    # w[68] = wz[6]
    # w[69] = wz[7]
    # w[70] = wz[8]
    # w[71] = wz[9]
    #4 TEST
    # v[52] = vz[0]
    # v[53] = vz[1]
    # v[54] = vz[2]
    # v[55] = vz[3]
    # v[56] = vz[4]
    # v[62] = vz[5]
    # v[63] = vz[6]
    # v[64] = vz[7]
    # v[65] = vz[8]
    # v[66] = vz[9]
    for k in range(0,N-1):
        Xnoise[k+1] = np.dot(Xnoise[k], F) + np.dot(PSI, Ut[k]) + np.dot(GAM, w[k])
        Xclear[k+1] = np.dot(Xclear[k], F) + np.dot(PSI, Ut[k])
        Yclear[k+1] = np.dot(Xclear[k], H.transpose())
        Ynoise[k+1] = np.dot(Xnoise[k+1], H.transpose()) + v[k+1]
    return Ynoise, Yclear, Xnoise

def input(fileName):
    try:
        file = open(fileName,'r')
    except IOError as e:
        print('не удалось открыть файл')
    else:
        with file:
            r = np.array([])
            R = np.array([])
            q = np.array([])
            Q = np.array([])
            x = np.array([])
            P = np.array([])
            t = 0
            u = np.array([])
            F = np.array([])
            H = np.array([])
            PSI = np.array([]) 
            GAM = np.array([])
            for line in file:
                if line[0] == '/':
                    Buff = line
                    continue
                else:
                    if Buff.find('/r_') != -1:
                        r = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/R_') != -1:
                        R = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/q_') != -1:
                        q = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/Q_') != -1:
                        Q = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/x_') != -1:
                        x = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/P_') != -1:
                        P = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/t_') != -1:
                        t = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/u_') != -1:
                        u = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/F_') != -1:
                        F = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/H_') != -1:
                        H = np.array(list(map(float, line.split(' '))))
                        continue   
                    if Buff.find('/PSI_') != -1:
                        PSI = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/GAM_') != -1:
                        GAM = np.array(list(map(float, line.split(' '))))
                        continue

            XdimArray = np.array([q, Q, x, P, u, F, PSI, GAM])
            YdimArray = [r, R]
            
            Nx = int(np.sqrt(len(P)))
            Ny = int(np.sqrt(len(R)))

            H.shape = (Ny, Nx)

            for variable in XdimArray:
                if len(variable) == 1:
                    variable.shape = (1,)
                elif len(variable) == Nx:
                    variable.shape = (Nx,)
                elif len(variable) == Nx**2:
                    variable.shape = (Nx,Nx)
            
            for variable in YdimArray:
                if len(variable) == 1:
                    variable.shape = (1,)
                elif len(variable) == Ny:
                    variable.shape = (Ny,)
                elif len(variable) == Ny**2:
                    variable.shape = (Ny,Ny)        
                    
            return r, R, q, Q, x, P, int(t), u, F, H, PSI, GAM, Nx, Ny

def AFilter(Ymodel, r, R, q, Q, Ex, P, N, Ut, F, H, PSI, GAM, Nx, Ny, param = 1):
    b = 0.998
    param = float(param)
    F[0][1] = param
    Bt = np.zeros((N,Ny,Ny))
    Xt = np.zeros((N,1,Nx))
    Xt[0] = Ex
    Pt = np.zeros((N,Nx,Nx))
    Pt[0] = P
    Qt = np.zeros((N,Nx,Nx))
    Qt[0] = Q
    Rt = np.zeros((N,Ny,Ny))
    Rt[0] = R
    Kt = np.zeros((N,Nx,Ny))
    Yresult = np.zeros((N,1,Ny))
    Yresult[0] = np.dot(H,Xt[0].transpose())
    eps = np.zeros((N,1,Ny))
    for k in range(N-1):
        Xt[k+1] = np.dot(Xt[k],F) + np.dot(Ut[k]*param, PSI)
        Pt[k+1] = np.dot(np.dot(F,Pt[k]),F.transpose()) + np.dot(np.dot(PSI,Qt[k]),PSI.transpose())
        
        Bt[k+1] = np.dot(np.dot(H, Pt[k+1]), H.transpose()) + Rt[k+1]

        d = (1-b)/(1-b**k+1)
        eps[k+1] = Ymodel[k+1] - np.dot(H,Xt[k+1].transpose())
        E = np.dot(np.linalg.inv(np.dot(GAM.transpose(), GAM)), GAM.transpose())
        ##++##
        Rt[k+1] = (1-d)*Rt[k]+d*np.dot((np.eye(Ny)-np.dot(H, Kt[k+1])), np.dot(np.dot(eps[k+1], eps[k+1].transpose()), (np.eye(Ny)-np.dot(H, Kt[k+1])).transpose())+np.dot(np.dot(H, Pt[k+1]),H.transpose()))
        
        Kt[k+1] = np.dot(np.dot(Pt[k+1], H.transpose()), np.linalg.inv(np.dot(np.dot(H, Pt[k+1]),H.transpose()) + np.dot(np.dot(GAM[0][0], Rt[k+1]), GAM[0][0])))
        Xt[k+1] += np.dot(Kt[k+1], eps[k+1]).transpose()
        Pt[k+1] = np.dot(np.eye(Nx) - np.dot(Kt[k+1], H), Pt[k+1])
        Yresult[k+1] = np.dot(H,Xt[k+1].transpose())
        Qt[k+1] = (1-d)*Qt[k]+np.dot(np.dot(d*E, np.dot(np.dot(np.dot(Kt[k+1], eps[k+1].transpose()), eps[k+1]), Kt[k+1].transpose())+Pt[k+1]-np.dot(np.dot(F,Pt[k]),F.transpose())), E.transpose())
    return {'x': Xt, 'y': Yresult, 'eps': eps, 'Pyy': Bt}

def Filter(Ymodel, r, R, q, Q, Ex, P, N, Ut, F, H, PSI, GAM, Nx, Ny, param = 1):
    b = 0.998
    param = float(param)
    F[0][1] = param
    Bt = np.zeros((N,Ny,Ny))
    Xt = np.zeros((N,1,Nx))
    Xt[0] = Ex
    Pt = np.zeros((N,Nx,Nx))
    Pt[0] = P
    Kt = np.zeros((N,Nx,Ny))
    Yresult = np.zeros((N,1,Ny))
    Yresult[0] = np.dot(H,Xt[0].transpose())
    eps = np.zeros((N,1,Ny))
    for k in range(N-1):
        Xt[k+1] = np.dot(Xt[k],F) + np.dot(Ut[k]*param, PSI)
        Pt[k+1] = np.dot(np.dot(F,Pt[k]),F.transpose()) + np.dot(np.dot(PSI,Q),PSI.transpose())
        
        Bt[k+1] = np.dot(np.dot(H, Pt[k+1]), H.transpose()) + R

        eps[k+1] = Ymodel[k+1] - np.dot(H,Xt[k+1].transpose())
        
        Kt[k+1] = np.dot(np.dot(Pt[k+1], H.transpose()), np.linalg.inv(np.dot(np.dot(H, Pt[k+1]),H.transpose()) + np.dot(np.dot(GAM[0][0], R), GAM[0][0])))
        Xt[k+1] += np.dot(Kt[k+1], eps[k+1]).transpose()
        Pt[k+1] = np.dot(np.eye(Nx) - np.dot(Kt[k+1], H), Pt[k+1])
        Yresult[k+1] = np.dot(H,Xt[k+1].transpose())
    return {'x': Xt, 'y': Yresult, 'eps': eps, 'Pyy': Bt}

def AlnL(param):
    filtred = AFilter(GYnoise, Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GF, GH, GPSI, GGAM, GNx, GNy, param)
    eps = filtred['eps']
    B = filtred['Pyy']
    print("NORM_E["+str(param)+"] = "+str(np.linalg.norm(eps)))
    result = -GN*GNy/2*np.log1p(2*np.pi)
    buff = 0
    for k in range(1,GN): 
        buff += np.dot(np.dot(np.column_stack(eps[k]), np.linalg.inv(B[k])), np.row_stack(eps[k]))
    result -= buff*0.5
    buff = 0
    for k in range(1,GN):
        buff += np.log1p(np.linalg.det(B[k]))
    result -= buff*0.5
    return -result

def lnL(param):
    filtred = Filter(GYnoise, Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GF, GH, GPSI, GGAM, GNx, GNy, param)
    eps = filtred['eps']
    B = filtred['Pyy']
    print("NORM_E["+str(param)+"] = "+str(np.linalg.norm(eps)))
    result = -GN*GNy/2*np.log1p(2*np.pi)
    buff = 0
    for k in range(1,GN): 
        buff += np.dot(np.dot(np.column_stack(eps[k]), np.linalg.inv(B[k])), np.row_stack(eps[k]))
    result -= buff*0.5
    buff = 0
    for k in range(1,GN):
        buff += np.log1p(np.linalg.det(B[k]))
    result -= buff*0.5
    return -result

def render(model = [], filtered = [], clear = []):
    plt.subplot(1, 1, 1)
    plt.plot(range(len(model)), model, 'b.:', label="Y model")
    plt.plot(range(len(filtered)), filtered, 'g.:', label="Y EFK")
    plt.plot(range(len(clear)), clear, 'r.:', label="Y without noise")
    plt.title('extended Kalman filter')
    plt.ylabel('Y')
    plt.grid(True)
    leg = plt.legend(loc='best', ncol=1, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.show()

def getNorm(values):
    norm = 0
    for i in range(GN):
        norm = GYclear[i] - values[i]
    return np.linalg.norm(norm)

main()