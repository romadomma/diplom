from model import f,h
from filter import UnscentedFilter, ExtendedFilter
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt

def init():
    global Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy, GYnoise, GYclear, GXnoise 
    Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy = input("input_ukf.txt")
    #Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy = input("/home/roman/Рабочий стол/diplom/input_ukf.txt")
    GYnoise, GYclear, GXnoise = GetModel(Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy)

def main():
    global Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy, GYnoise, GYclear, GXnoise
    init()
    # filtred = ExtendedFilter(GYnoise, Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy)
    # filtredU = UnscentedFilter(GYnoise, Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy)
    # print("NORM_E = "+str(getNorm(filtred['y'][:,0])))
    # render(GYnoise[:,0], filtred['y'][:,0], GYclear[:,0])
    # print("NORM_U = "+str(getNorm(filtredU['y'][:,0])))
    # renderU(GYnoise[:,0], filtredU['y'][:,0], GYclear[:,0])
    # optimal = opt.minimize_scalar(lnL)
    # optimalU = opt.minimize_scalar(lnLU)
    # filtred = ExtendedFilter(GYnoise, Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy, param = optimal['x'])
    # filtredU = UnscentedFilter(GYnoise, Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy, param = optimal['x'])
    # print(optimal)
    # print(1-optimal['x'])
    # print(getNorm(filtred['y'][:,0])/np.linalg.norm(GYclear))
    # print(optimalU)
    # print(1-optimalU['x'])
    # print(getNorm(filtredU['y'][:,0])/np.linalg.norm(GYclear))
    renderLnL()

def render(model = [], filtered = [], clear = []):
    plt.subplot(1, 1, 1)
    plt.plot(range(len(model)), model, 'b.:', label="Y model")
    plt.plot(range(len(filtered)), filtered, 'g.:', label="Y EFK")
    plt.plot(range(len(clear)), clear, 'r.:', label="Y without noise")
    plt.title('extended Kalman filter')
    plt.ylabel('Y')
    plt.grid(True)
    leg = plt.legend(loc='best', ncol=1, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.show()

def renderU(model = [], filtredU =[], clear = []):
    plt.subplot(1, 1, 1)
    plt.plot(range(len(model)), model, 'b.:', label="Y model")
    plt.plot(range(len(filtredU)), filtredU, 'g.:', label="Y UFK")
    plt.plot(range(len(clear)), clear, 'r.:', label="Y without noise")
    plt.title('unscented Kalman filter')
    plt.ylabel('Y')
    plt.grid(True)
    leg = plt.legend(loc='best', ncol=1, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.show()

def getNorm(values):
    norm = 0
    for i in range(GN):
        norm = GYclear[i] - values[i]
    return np.linalg.norm(norm)

def renderLnL():
    values = []
    for i in np.arange(-2,2,0.01):
        values = np.append(values, lnL(i))
    plt.subplot(1,1,1)
    plt.plot(np.arange(-2,2,0.01), values, 'b-', label="lnL(param)")
    plt.title('lnL(param)')
    plt.ylabel('LnL')
    plt.grid(True)
    leg = plt.legend(loc='best', ncol=1, fancybox=True)
    leg.get_frame().set_alpha(0.5)
    plt.show()

def lnL(param):
    # filtred = UnscentedFilter(GYnoise, Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy, param)
    filtred = ExtendedFilter(GYnoise, Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy, param)
    eps = filtred['eps']
    B = filtred['Pyy']
    print("NORM_E["+str(param)+"] = "+str(np.linalg.norm(eps)))
    result = -GN*GNy/2*np.log1p(2*np.pi)
    buff = 0
    for k in range(1,GN): 
        buff += np.dot(np.dot(np.column_stack(eps[k]), np.linalg.inv(B[k])), np.row_stack(eps[k]))
    result -= buff*0.5
    buff = 0
    for k in range(1,GN):
        buff += np.log1p(np.linalg.det(B[k]))
    result -= buff*0.5
    return -result
 
def input(fileName):
    try:
        file = open(fileName,'r')
    except IOError as e:
        print('не удалось открыть файл')
    else:
        with file:
            r = np.array([])
            R = np.array([])
            q = np.array([])
            Q = np.array([])
            x = np.array([])
            P = np.array([])
            t = 0
            u = np.array([])

            for line in file:
                if line[0] == '/':
                    Buff = line
                    continue
                else:
                    if Buff.find('/r_') != -1:
                        r = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/R_') != -1:
                        R = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/q_') != -1:
                        q = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/Q_') != -1:
                        Q = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/x_') != -1:
                        x = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/P_') != -1:
                        P = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/t_') != -1:
                        t = np.array(list(map(float, line.split(' '))))
                        continue
                    if Buff.find('/u_') != -1:
                        u = np.array(list(map(float, line.split(' '))))
                        continue
            
            XdimArray = np.array([q, Q, x, P, u])
            YdimArray = [r, R]
            
            Nx = int(np.sqrt(len(P)))
            Ny = int(np.sqrt(len(R)))
            
            for variable in XdimArray:
                if len(variable) == 1:
                    variable.shape = (1,)
                elif len(variable) == Nx:
                    variable.shape = (Nx,)
                elif len(variable) == Nx**2:
                    variable.shape = (Nx,Nx)
            
            for variable in YdimArray:
                if len(variable) == 1:
                    variable.shape = (1,)
                elif len(variable) == Ny:
                    variable.shape = (Ny,)
                elif len(variable) == Ny**2:
                    variable.shape = (Ny,Ny)        
                    
            return r, R, q, Q, x, P, int(t), u, Nx, Ny 

def GetModel(r, R, q, Q, Ex, P, N, u, Nx, Ny):
    global Gr, GR, Gq, GQ, GEx, GP, GN, Gu, GNx, GNy, GYnoise, GYclear, GXnoise
    Yclear = np.zeros((N,1,Ny))
    Xclear = np.zeros((N,1,Nx))
    Xnoise = np.zeros((N,1,Nx))
    Ynoise = np.zeros((N,1,Ny))
    Xnoise[0] = np.random.multivariate_normal(Ex, P, 1) if Nx > 1 else np.random.normal(Ex, P, 1) 
    Xclear[0] = Xnoise[0]
    Ynoise[0][0] = h(Xnoise[0])
    Yclear[0][0] = h(Xclear[0])
    w = np.random.multivariate_normal(q, Q, N) if Nx > 1 else np.random.normal(q, Q, N) 
    v = np.random.multivariate_normal(r, R, N) if Ny > 1 else np.random.normal(r, R, N)

    Qz = np.array([[1, 0], [0, 1]])
    Rz = np.array([1])
    wz = np.random.multivariate_normal(q, Qz, 10) if Nx > 1 else np.random.normal(q, Qz, 10) 
    vz = np.random.multivariate_normal(r, Rz, 10) if Ny > 1 else np.random.normal(r, Rz, 10)

    #1 TEST
    # w[13] = wz[0]
    # w[22] = wz[1]
    # w[29] = wz[2]
    # w[35] = wz[3]
    # w[42] = wz[4]
    # w[50] = wz[5]
    # w[63] = wz[6]
    # w[81] = wz[7]
    # w[88] = wz[8]
    # w[97] = wz[9]
    #2 TEST
    # v[4] = vz[0]
    # v[15] = vz[1]
    # v[19] = vz[2]
    # v[26] = vz[3]
    # v[34] = vz[4]
    # v[46] = vz[5]
    # v[57] = vz[6]
    # v[69] = vz[7]
    # v[74] = vz[8]
    # v[84] = vz[9]
    #3 TEST
    # w[21] = wz[0]
    # w[22] = wz[1]
    # w[23] = wz[2]
    # w[24] = wz[3]
    # w[25] = wz[4]
    # w[67] = wz[5]
    # w[68] = wz[6]
    # w[69] = wz[7]
    # w[70] = wz[8]
    # w[71] = wz[9]
    #4 TEST
    # v[52] = vz[0]
    # v[53] = vz[1]
    # v[54] = vz[2]
    # v[55] = vz[3]
    # v[56] = vz[4]
    # v[62] = vz[5]
    # v[63] = vz[6]
    # v[64] = vz[7]
    # v[65] = vz[8]
    # v[66] = vz[9]
    for k in range(1,N):
        Xnoise[k] = f(Xnoise[k-1],u[k],w = w[k])
        Xclear[k] = f(Xclear[k-1],u[k])
        Yclear[k] = h(Xclear[k])
        Ynoise[k] = h(Xnoise[k],v = v[k])
    return Ynoise, Yclear, Xnoise
    
main()